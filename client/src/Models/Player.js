import isEmpty from "lodash/isEmpty";
import orderBy from "lodash/orderBy";
import pick from "lodash/pick";
import map from "lodash/map";
import groupBy from "lodash/groupBy";
import moment from "moment";

export const columns = [
    {
        name: "id",
        label: "Number"
    },
    {
        name: "name",
        label: "Name"
    },
    {
        name: "pos",
        label: "Position"
    },
    {
        name: "nat",
        label: "Nationality"
    },
    {
        name: "height",
        label: "Height"
    },
    {
        name: "weight",
        label: "Weight"
    },
    {
        name: "dob",
        label: "Date of Birth",
        formatter: val => val.format("L")
    },
    {
        name: "birthplace",
        label: "Birthplace"
    }
];

export function mapPlayer(player) {
    const { dob } = player;
    return {
        ...player,
        dob: moment({ year: dob[0], month: dob[1] - 1, date: dob[2] })
    };
}

export function selectPlayers(players, filter, sorting) {
    const filterFunction = getFilterFunction(filter);
    return orderBy(
        players.filter(filterFunction),
        sorting.column,
        sorting.ascending ? "asc" : "desc"
    );
}

const textProperties = ["name", "pos", "nat", "birthplace"];

function getFilterFunction(filter) {
    if (isEmpty(filter)) return () => true;
    const filterExpression = filter.toLowerCase();
    return player =>
        map(pick(player, textProperties), preparePropertyForFiltering).some(
            p => p && p.includes(filterExpression)
        );
}

function preparePropertyForFiltering(property) {
    return property && property.toLowerCase();
}

export function getPlayerStats(players, groupByProperty) {
    const groups = map(groupBy(players, groupByProperty), (val, prop) => ({
        key: prop,
        count: val.length
    }));
    return orderBy(groups, "key");
}
