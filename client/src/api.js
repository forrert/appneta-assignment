import axios from "axios";

import { mapPlayer } from "./Models/Player";

export const getPlayers = () =>
    axios.get("/players").then(response => response.data.map(mapPlayer));
