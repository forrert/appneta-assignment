import React, { Component } from "react";
import "./App.css";
import Players from "./Components/Players";

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">AppNeta</h1>
                </header>
                <div className="App-intro">
                    <Players />
                </div>
            </div>
        );
    }
}

export default App;
