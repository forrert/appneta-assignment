import React from "react";
import PropTypes from "prop-types";
import { Table } from "reactstrap";

function DataTable(props) {
    const {
        columns,
        records,
        onColumnClick,
        noRecordsMessage,
        sorting
    } = props;
    return (
        <Table striped bordered>
            <TableHeader
                columns={columns}
                onColumnClick={onColumnClick}
                sorting={sorting}
            />
            <TableBody
                columns={columns}
                records={records}
                noRecordsMessage={noRecordsMessage}
            />
        </Table>
    );
}

function TableHeader(props) {
    const { columns, onColumnClick, sorting } = props;
    return (
        <thead>
            <tr>
                {columns.map(c => (
                    <th key={c.name} onClick={() => onColumnClick(c)}>
                        {c.label}
                        <SortingIndicator column={c} sorting={sorting} />
                    </th>
                ))}
            </tr>
        </thead>
    );
}

const upArrow = "▲";
const downArrow = "▼";

function SortingIndicator(props) {
    const { column, sorting } = props;
    let indicator = `${upArrow} ${downArrow}`;
    if (sorting.column === column.name) {
        indicator = sorting.ascending ? upArrow : downArrow;
    }
    return (
        <div className="float-right">
            <small>{indicator}</small>
        </div>
    );
}

function TableBody(props) {
    const { columns, records, noRecordsMessage } = props;
    if (records.length === 0)
        return (
            <tbody>
                <NoRecords
                    columns={columns}
                    noRecordsMessage={noRecordsMessage}
                />
            </tbody>
        );
    return (
        <tbody>
            {records.map((record, index) => (
                <DataRow key={index} record={record} columns={columns} />
            ))}
        </tbody>
    );
}

function DataRow(props) {
    const { columns, record } = props;
    return (
        <tr>
            {columns.map(column => (
                <DataCell key={column.name} column={column} record={record} />
            ))}
        </tr>
    );
}

function DataCell(props) {
    const { column, record } = props;
    const value = record[column.name];
    let displayValue = value;
    if (column.formatter) {
        displayValue = column.formatter(value);
    }
    return <td>{displayValue}</td>;
}

function NoRecords(props) {
    const { columns, noRecordsMessage } = props;
    return (
        <tr>
            <td colSpan={columns.length}>{noRecordsMessage}</td>
        </tr>
    );
}

DataTable.propTypes = {
    records: PropTypes.array.isRequired,
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
            formatter: PropTypes.func
        })
    ).isRequired,
    onColumnClick: PropTypes.func.isRequired,
    sorting: PropTypes.shape({
        column: PropTypes.string,
        ascending: PropTypes.bool
    }).isRequired,
    noRecordsMessage: PropTypes.string
};

DataTable.defaultProps = {
    noRecordsMessage: "No records"
};

export default DataTable;
