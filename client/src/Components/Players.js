import React, { Component } from "react";

import { getPlayers } from "../api";
import { selectPlayers } from "../Models/Player";
import PlayerList from "./PlayerList";
import PlayerFilter from "./PlayerFilter";
import PlayerSummary from "./PlayerSummary";

class Players extends Component {
    state = {
        loading: false,
        sorting: {
            column: "id",
            ascending: true
        },
        filter: "",
        players: []
    };

    render() {
        const { sorting, filter, err, loading } = this.state;
        if (err) return "Error loading data.";
        if (loading) return "Loading data...";
        const players = this.getPlayers();
        return (
            <div>
                <PlayerFilter filter={filter} onFilterChange={this.setFilter} />
                <PlayerSummary players={players} groupByProperty="nat" />
                <PlayerList
                    players={players}
                    onColumnClick={this.setSorting}
                    sorting={sorting}
                />
            </div>
        );
    }

    getPlayers() {
        const { players, sorting, filter } = this.state;
        return selectPlayers(players, filter, sorting);
    }

    componentDidMount() {
        this.setState({ loading: true });
        getPlayers()
            .then(players => {
                this.setState({ players, loading: false });
            })
            .catch(err => {
                this.setState({ error: err });
            });
    }

    setSorting = column => {
        const { sorting } = this.state;
        let newSorting = { column: column.name, ascending: true };
        if (sorting.column === column.name) {
            newSorting.ascending = !sorting.ascending;
        }
        this.setState({ sorting: newSorting });
    };

    setFilter = filter => {
        this.setState({ filter });
    };
}

export default Players;
