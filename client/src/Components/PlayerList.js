import React from "react";
import PropTypes from "prop-types";

import DataTable from "./DataTable";
import { columns } from "../Models/Player";

function PlayerList(props) {
    const { players, onColumnClick, sorting } = props;
    return (
        <DataTable
            columns={columns}
            records={players}
            onColumnClick={onColumnClick}
            sorting={sorting}
        />
    );
}

PlayerList.propTypes = {
    players: PropTypes.array.isRequired,
    onColumnClick: PropTypes.func,
    sorting: PropTypes.shape({
        column: PropTypes.string,
        ascending: PropTypes.bool
    })
};

export default PlayerList;
