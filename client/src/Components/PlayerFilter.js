import React from "react";
import { Form, InputGroup, InputGroupAddon, Input } from "reactstrap";

function PlayerFilter(props) {
    const { filter, onFilterChange } = props;
    return (
        <Form className="search-form">
            <InputGroup>
                <InputGroupAddon addonType="prepend">&#x1F50D;</InputGroupAddon>
                <Input
                    value={filter}
                    placeholder="Search"
                    onChange={e => onFilterChange(e.target.value)}
                />
            </InputGroup>
        </Form>
    );
}

export default PlayerFilter;
