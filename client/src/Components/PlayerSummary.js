import React from "react";
import PropTypes from "prop-types";
import map from "lodash/map";
import isEmpty from "lodash/isEmpty";
import { Table } from "reactstrap";

import { getPlayerStats } from "../Models/Player";

export default function PlayerSummary(props) {
    const { players, groupByProperty } = props;
    const stats = getPlayerStats(players, groupByProperty);
    return (
        <Table bordered>
            <tbody>
                <tr>
                    <StatsCells stats={stats} />
                </tr>
            </tbody>
        </Table>
    );
}

PlayerSummary.propTypes = {
    players: PropTypes.array.isRequired,
    groupByProperty: PropTypes.string.isRequired
};

function StatsCells(props) {
    const { stats } = props;
    if (isEmpty(stats)) {
        return <td>No records</td>;
    }
    return map(stats, group => (
        <StatsCell key={group.key} property={group.key} count={group.count} />
    ));
}

function StatsCell(props) {
    const { property, count } = props;
    return (
        <td>
            {property}: {count}
        </td>
    );
}
