package com.example.helloworld.resources;

import com.codahale.metrics.annotation.Timed;
import com.example.helloworld.core.Player;
import com.example.helloworld.db.PlayerDAO;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/players")
@Produces(MediaType.APPLICATION_JSON)
public class PlayersResource {
    private final PlayerDAO dao;

    public PlayersResource(PlayerDAO dao) {
        this.dao = dao;
    }

    @GET
    @Timed(name = "get-requests")
    @UnitOfWork
    public Iterable<Player> getPlayers() {
        return dao.findAll();
    }
}
